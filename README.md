# Project Shield Documentation

This repository is for [PlantUML](http://plantuml.com) documentation related to development, branching and releasing processes.

---

## Getting Started 

### Requirements

Install the following tools using the following commands, assuming you have [Brew](https://brew.sh/) installed.

```bash
brew install plantuml
brew install --cask docker
```

---

## Generate Images

Running this script will generate new PNG and SVG images for each PlantUML document in this repository.  Please run this before committing your work.

```bash
./generate-diagrams.sh
```

---

## Help Me With PlantUML

If you aren't [Ross](https://media.giphy.com/media/2wUzjQMSsQKZzLdWj4/giphy.gif) and you need to learn PlantUML have a look at the following resources.

1. [PlantUML Guide](http://plantuml.com/guide)
1. [PlantUML Sequence Diagrams](https://plantuml.com/sequence-diagram)
1. [PlantUML Documentation](https://ogom.github.io/draw_uml/plantuml/)
1. [PlantUML Cheat Sheet](https://blog.anoff.io/puml-cheatsheet.pdf)